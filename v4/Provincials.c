#pragma config(Sensor, in4,    lightindexer,   sensorReflection)
#pragma config(Sensor, in5,    potarm1,        sensorPotentiometer)
#pragma config(Sensor, in6,    gyro,           sensorGyro)
#pragma config(Sensor, dgtl1,  armenc,         sensorQuadEncoder)
#pragma config(Sensor, dgtl4,  indexbtn,       sensorTouch)
#pragma config(Sensor, dgtl5,  rightdrive_enc, sensorQuadEncoder)
#pragma config(Sensor, dgtl7,  leftdrive_enc,  sensorQuadEncoder)
#pragma config(Sensor, dgtl11, flywheelenc,    sensorQuadEncoder)
#pragma config(Motor,  port1,           indexer,       tmotorVex393_HBridge, openLoop)
#pragma config(Motor,  port2,           drive_r1,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port3,           flywheel1,     tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port4,           drive_r2,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port5,           arm_cap1,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port6,           arm_cap2,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port7,           drive_l2,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port8,           drive_l1,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port9,           flywheel2,     tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port10,          intake, tmotorVex393_HBridge, openLoop)
#pragma config(Sensor, in3,    potentiometerLCD, sensorPotentiometer)
#pragma config(Sensor, in2,    secondaryBattery, sensorAnalog)

//!IMPORTANT -> Comment out this line to disable competition mode
#define competitionEnabled


/* Some constant definitions used in the code */

/* These definitions put a name behind the IDs that each LCD button represents */
#define isLCDEnabled					true
#define LCDLeftButton					1
#define LCDCenterButton					2
#define LCDRightButton					4

/* Definitions representing the different sides and colors on the field.
Each Side/Color is represented by a boolean */
#define COLOR_BLUE	false
#define COLOR_RED	true
#define SIDE_LEFT	false
#define SIDE_RIGHT	true

#define IS_CONTROL_LOCK_ENABLED		false

/* Button Definitions Associate Functional Name With Button Reference */
#define BTN_JOY_LCD_SELECT				Btn6U
#define BTN_JOY_LCD_PREVIOUS			Btn8L
#define BTN_JOY_LCD_NEXT				Btn8R

#define JOY_LCD_X		Ch1
#define JOY_LCD_Y		Ch2

/* Number of items in each menu list */
#define MENU_LIST_MAIN_LENGTH				6
#define MENU_LIST_AUTON_LENGTH 			4
#define MENU_LIST_SIDES_LENGTH 			3
#define MENU_LIST_COLORS_LENGTH			3

/* Deadzones */
#define	LCD_JOYSTICK_DEADZONE	20

/* MenuItem Struct */
struct MenuItem {
	short id;
	short idx;
	string name;
	string LCDAction;
	bool isDirectional;
};


//main screen menu items
MenuItem menuItemUserControl,  menuItemSwitchCompetitionMode, menuItemGoToAuton, menuItemResetGyro, menuItemBatteryLevel, menuItemCurrentProgram;
//autonomous menu items
MenuItem menuItemAutonGoBack, menuItemAutonFront, menuItemAutonBack, menuItemAutonNone;
//side menu items
MenuItem menuItemSideGoBack, menuItemSideLeft, menuItemSideRight;
//color menu items
MenuItem menuItemColorGoBack, menuItemColorBlue, menuItemColorRed;

//screen lists for LCD
MenuItem* menuListMain[MENU_LIST_MAIN_LENGTH];
MenuItem* menuListAuton[MENU_LIST_AUTON_LENGTH];
MenuItem* menuListSides[MENU_LIST_SIDES_LENGTH];
MenuItem* menuListColors[MENU_LIST_COLORS_LENGTH];

MenuItem* currentMenu;

MenuItem *startupLCDProgram = &menuItemUserControl;











/* add all the menuItems to their respective lists by reference
and assign ids and indices to each item */
void populateMenuItems() {
	ubyte i = 0;
	ubyte id = 0;

	menuItemUserControl.name = "User Control";
	menuItemUserControl.LCDAction = "Start";
	menuItemUserControl.idx = i;
	menuItemUserControl.id = id;
	menuListMain[i] = &menuItemUserControl;

	menuItemSwitchCompetitionMode.name = "Switch Comp. Mode";
	menuItemSwitchCompetitionMode.id = ++id;
	menuItemSwitchCompetitionMode.idx = ++i;
	menuItemSwitchCompetitionMode.LCDAction = "Select";
	menuListMain[i] = &menuItemSwitchCompetitionMode;

	menuItemGoToAuton.name = "Go To Autonomous";
	menuItemGoToAuton.id = ++id;
	menuItemGoToAuton.idx = ++i;
	menuItemGoToAuton.LCDAction = "Select";
	menuListMain[i] = &menuItemGoToAuton;

	menuItemResetGyro.name = "Reset Gyro";
	menuItemResetGyro.id = ++id;
	menuItemResetGyro.idx = ++i;
	menuItemResetGyro.LCDAction = "Reset";
	menuListMain[i] = &menuItemResetGyro;

	menuItemBatteryLevel.name = "Battery Level";
	menuItemBatteryLevel.id = ++id;
	menuItemBatteryLevel.idx = ++i;
	menuItemBatteryLevel.LCDAction = "Select";
	menuListMain[i] = &menuItemBatteryLevel;

	menuItemCurrentProgram.name = "Current Program";
	menuItemCurrentProgram.id = ++id;
	menuItemCurrentProgram.idx = ++i;
	menuListMain[i] = &menuItemCurrentProgram;

	i = 0;

	menuItemAutonGoBack.name = "Go Back";
	menuItemAutonGoBack.id = ++id;
	menuItemAutonGoBack.idx = i;
	menuItemAutonGoBack.LCDAction = "Select";
	menuListAuton[i] = &menuItemAutonGoBack;

	menuItemAutonFront.name = "AutonFront";
	menuItemAutonFront.id = ++id;
	menuItemAutonFront.idx = ++i;
	menuItemAutonFront.isDirectional = true;
	menuListAuton[i] = &menuItemAutonFront;

	menuItemAutonBack.name = "AutonBack";
	menuItemAutonBack.id = ++id;
	menuItemAutonBack.idx = ++i;
	menuItemAutonBack.isDirectional = true;
	menuListAuton[i] = &menuItemAutonBack;


	menuItemAutonNone.name = "AutonNone";
	menuItemAutonNone.id = ++id;
	menuItemAutonNone.idx = ++i;
	menuItemAutonNone.isDirectional = true;
	menuListAuton[i] = &menuItemAutonNone;

	i = 0;

	menuItemSideGoBack.name = "Go Back";
	menuItemSideGoBack.id = ++id;
	menuItemSideGoBack.idx = i;
	menuItemSideGoBack.LCDAction = "Select";
	menuListSides[i] = &menuItemSideGoBack;

	menuItemSideLeft.name = "L";
	menuItemSideLeft.id = ++id;
	menuItemSideLeft.idx = ++i;
	menuItemSideLeft.LCDAction = "Select";
	menuListSides[i] = &menuItemSideLeft;

	menuItemSideRight.name = "R";
	menuItemSideRight.id = ++id;
	menuItemSideRight.idx = ++i;
	menuItemSideRight.LCDAction = "Select";
	menuListSides[i] = &menuItemSideRight;

	i = 0;

	menuItemColorGoBack.name = "Go Back";
	menuItemColorGoBack.id = ++id;
	menuItemColorGoBack.idx = i;
	menuItemColorGoBack.LCDAction = "Select";
	menuListColors[i] = &menuItemColorGoBack;

	menuItemColorBlue.name = "B";
	menuItemColorBlue.id = ++id;
	menuItemColorBlue.idx = ++i;
	menuItemColorBlue.LCDAction = "Select";
	menuListColors[i] = &menuItemColorBlue;

	menuItemColorRed.name = "R";
	menuItemColorRed.id = ++id;
	menuItemColorRed.idx = ++i;
	menuItemColorRed.LCDAction = "Select";
	menuListColors[i] = &menuItemColorRed;

	i = 0;

	currentMenu = menuListMain;
}




/************************/
/*						*/
/* VEX COMPETITION CODE */
/*						*/
/************************/

#pragma platform(VEX2)
#pragma competitionControl(Competition)

void pre_auton();
task autonomous();
task usercontrol();

bool bStopTasksBetweenModes = true;
#if !defined(VEX2) && !defined(VEX)
#error "Switch to the VEX cortex platform"
#endif

void allMotorsOff();
void stopTasks();
bool isCompetitionMode = false;

task main()
{
	clearLCDLine(0);
	clearLCDLine(1);
	displayLCDPos(0, 0);
	displayNextLCDString("Startup");
	wait1Msec(2000);
	clearLCDLine(0);

	pre_auton();

	while (true)
	{
		if (isCompetitionMode)
		{
			// remain in this loop while the robot is disabled
			while (bIfiRobotDisabled)
			{
				while (true)
				{
					if (!bIfiRobotDisabled)
						break;
					wait1Msec(25);
				}
			}

			if (bIfiAutonomousMode)
			{
				startTask(autonomous);

				// Waiting for autonomous phase to end
				while (bIfiAutonomousMode && !bIfiRobotDisabled)
				{
					if (!bVEXNETActive)
					{
						if (nVexRCReceiveState == vrNoXmiters) // the transmitters are powered off!!
							allMotorsOff();
					}
					wait1Msec(25);               // Waiting for autonomous phase to end
				}
				allMotorsOff();
				if (bStopTasksBetweenModes)
				{
					stopTasks();
				}
			}

			else
			{
				startTask(usercontrol);

				// Here we repeat loop waiting for user control to end and (optionally) start
				// of a new competition run
				while (!bIfiAutonomousMode && !bIfiRobotDisabled)
				{
					if (nVexRCReceiveState == vrNoXmiters) // the transmitters are powered off!!
						allMotorsOff();
					wait1Msec(25);
				}
				allMotorsOff();
				if (bStopTasksBetweenModes)
				{
					stopTasks();
				}
			}
		}
	}
}

void allMotorsOff()
{
	motor[port1] = 0;
	motor[port2] = 0;
	motor[port3] = 0;
	motor[port4] = 0;
	motor[port5] = 0;
	motor[port6] = 0;
	motor[port7] = 0;
	motor[port8] = 0;
#if defined(VEX2)
	motor[port9] = 0;
	motor[port10] = 0;
#endif
}





/*---------------------------------------------------------------------------*/
/*																			 */
/*							Pre-Autonomous Functions						 */
/*																			 */
/*---------------------------------------------------------------------------*/

/* references to procedures and tasks */
void startUp();
task loadLCDScreen();
task usercontrol();

MenuItem *selectedProgram;
ubyte LCDScreenMin = menuItemGoToAuton.idx; //minimum index for Screen List
ubyte LCDScreenMax = menuItemBatteryLevel.idx; //maximum index for Screen List
ubyte LCDScreen = 0; //current index of menuItem
bool autonomousSide = SIDE_LEFT; //side selected for autonomous
bool autonomousColor = COLOR_BLUE;

/* What the second line of the LCD should display on a menuItem */
string LCDAction = "<     Start    >";

/* if  this button is pressed the LCD will be controllable from the joystick */
bool isJoystickLCDMode() {
	return (vexRT[Btn7R] == 1);
}


void waitForLCDButtonRelease()
{
	//while these buttons are being pressed, keep waiting
	while (!(nLCDButtons == 0 && vexRT[BTN_JOY_LCD_PREVIOUS] == 0 && vexRT[BTN_JOY_LCD_SELECT] == 0 && vexRT[BTN_JOY_LCD_NEXT] == 0))
	{
		wait1Msec(10);
	}
}

float getRadAngleFromTanRatio(float y, float x)
{
	if (x == 0) x = 0.001;
	float angle = atan(abs(y) / abs(x));

	if (x > 0 && y > 0) return angle;
	else if (x < 0 && y > 0) return PI - angle;
	else if (x < 0 && y < 0) return PI + angle;
	else if (x > 0 && y < 0) return 2.0*PI - angle;

	return angle;
}



void reconfigureMenu(void* menuList, ubyte newLCDScreen, ubyte newLCDScreenMin, ubyte newLCDScreenMax)
{
	currentMenu = menuList;
	LCDScreen = newLCDScreen;
	LCDScreenMin = newLCDScreenMin;
	LCDScreenMax = newLCDScreenMax;
}


void waitForLCDButtonPress()
{
	int lastLCDPotValue = SensorValue[potentiometerLCD];
	while (abs(SensorValue[potentiometerLCD] - lastLCDPotValue) < 6 && nLCDButtons == 0 && (!isJoystickLCDMode() || (vexRT[BTN_JOY_LCD_PREVIOUS] == 0 && vexRT[BTN_JOY_LCD_SELECT] == 0 && vexRT[BTN_JOY_LCD_NEXT] == 0 && abs(vexRT[JOY_LCD_X]) < LCD_JOYSTICK_DEADZONE && abs(vexRT[JOY_LCD_Y]) < LCD_JOYSTICK_DEADZONE)))
	{
		lastLCDPotValue = SensorValue[potentiometerLCD];
		wait1Msec(10);
	}

	if (abs(vexRT[JOY_LCD_X]) > LCD_JOYSTICK_DEADZONE || abs(vexRT[JOY_LCD_Y]) > LCD_JOYSTICK_DEADZONE)
	{
		LCDScreen = floor(getRadAngleFromTanRatio(vexRT[JOY_LCD_Y], vexRT[JOY_LCD_X]) / (2.0*PI / (float)(LCDScreenMax - LCDScreenMin + 1)));
	}
	else if (abs(lastLCDPotValue - SensorValue[potentiometerLCD]) > 6)
	{
		reconfigureMenu(menuListAuton, 1, 0, MENU_LIST_AUTON_LENGTH - 1);
		LCDScreen = LCDScreenMin + floor((4095.0 - SensorValue[potentiometerLCD]) / (4095.0 / ((float)(LCDScreenMax - LCDScreenMin + 1))));
	}
}


/* Increment the index or jump to start of list */
void LCDNextScreen()
{
	if (LCDScreen < LCDScreenMax) LCDScreen++;
	else LCDScreen = LCDScreenMin;
}

/* Decrement the index or jump to end of list */
void LCDPreviousScreen()
{
	if (LCDScreen > LCDScreenMin) LCDScreen--;
	else LCDScreen = LCDScreenMax;
}


void displayBatteryLevelOnLCD()
{
	clearLCDLine(0);
	clearLCDLine(1);
	string mainBattery, backupBattery;

	sprintf(mainBattery, "%1.2f%c", nImmediateBatteryLevel / 1000.0, 'V');
	sprintf(backupBattery, "%1.2f%c", SensorValue[secondaryBattery] / 280.0, 'V');

	displayLCDString(0, 0, "Primary: ");
	displayNextLCDString(mainBattery);
	displayLCDString(1, 0, "Secondary: ");
	displayNextLCDString(backupBattery);
}

short getGyroSensorValue();



void displayProgram()
{
	string programName = (currentMenu[LCDScreen]).name; //name of displayed menuItem

	/* if current menu screen is the battery level screen, display battery level */
	if (currentMenu[LCDScreen].id == menuItemBatteryLevel.id)
	{
		displayBatteryLevelOnLCD();
	}
	/* if current menu screen is on the current program screen, display its name */
	else if (currentMenu[LCDScreen].id == menuItemCurrentProgram.id)
	{
		displayLCDCenteredString(0, "Daud-_-Jaan");
		displayLCDCenteredString(1, (*selectedProgram).name);
	}
	/* If the current menu is the directions menu add an L or an R to the end of the auton program name to indicate direction */
	else if (currentMenu == menuListSides && (currentMenu[LCDScreen].id == menuItemSideLeft.id || currentMenu[LCDScreen].id == menuItemSideRight.id))
	{
		programName = (*selectedProgram).name;
		if (currentMenu[LCDScreen].id == menuItemSideLeft.id) programName += "L";
		else programName += "R";
		displayLCDCenteredString(0, programName);
	}
	else if (currentMenu == menuListColors && (currentMenu[LCDScreen].id == menuItemColorBlue.id || currentMenu[LCDScreen].id == menuItemColorRed.id))
	{
		programName = (*selectedProgram).name;
		if (currentMenu[LCDScreen].id == menuItemColorBlue.id) programName += "B";
		else programName += "R";
		displayLCDCenteredString(0, programName);
	}
	/* If the current menu item is strictly a navigation item, change the second line to read "Select" */
	else if (currentMenu[LCDScreen].isDirectional)
	{
		displayLCDCenteredString(0, currentMenu[LCDScreen].name); //display menuItem name
		displayLCDCenteredString(1, "<    Select    >");
	}
	else if (currentMenu[LCDScreen].id == menuItemResetGyro.id)
	{
		displayLCDCenteredString(0, currentMenu[LCDScreen].name); //display menuItem name
		waitForLCDButtonRelease();
		string gyroValue;
		displayLCDString(1, 0, "< Reset (");
		/* Get value of gyro sensor and print it to the LCD Screen */
		while (nLCDButtons == 0 && vexRT[BTN_JOY_LCD_NEXT] == 0 && vexRT[BTN_JOY_LCD_PREVIOUS] == 0 && vexRT[BTN_JOY_LCD_SELECT] == 0)
		{
			sprintf(gyroValue, "%4.0f%c", getGyroSensorValue()); //convert to string format to 4 digits
			gyroValue += ")";
			displayLCDString(1, 9, gyroValue);
			displayLCDString(1, 15, ">");

			if (abs(vexRT[JOY_LCD_X]) > LCD_JOYSTICK_DEADZONE && abs(vexRT[JOY_LCD_Y]) > LCD_JOYSTICK_DEADZONE) break;
		}
	}
	/* Display the current mode of the Robot when on this menu screen. */
	else if (currentMenu[LCDScreen].id == menuItemSwitchCompetitionMode.id)
	{
		if (isCompetitionMode) displayLCDCenteredString(0, "Comp Mode Active");
		else displayLCDCenteredString(0, "Test Mode Active");
		displayLCDCenteredString(1, "Switch  Mode");
	}
	else
	{
		displayLCDCenteredString(0, currentMenu[LCDScreen].name); //display menuItem name
		displayLCDCenteredString(1, LCDAction); //display possible action
	}

	if (currentMenu[LCDScreen].id != menuItemResetGyro.id && currentMenu[LCDScreen].id != menuItemCurrentProgram.id && currentMenu[LCDScreen].id != menuItemBatteryLevel.id)
	{
		displayLCDString(1, 0, "<");
		displayLCDString(1, 15, ">");
	}

	waitForLCDButtonPress();

	/* If the Left and Right LCD Buttons are pressed, display the battery level. */
	if (nLCDButtons == LCDLeftButton + LCDRightButton || (vexRT[BTN_JOY_LCD_NEXT] == 1 && vexRT[BTN_JOY_LCD_PREVIOUS] == 1))
	{
		while (nLCDButtons == LCDLeftButton + LCDRightButton || (vexRT[BTN_JOY_LCD_NEXT] == 1 && vexRT[BTN_JOY_LCD_PREVIOUS] == 1))
		{
			displayBatteryLevelOnLCD();
		}
	}
	/* If the left LCD Button is pressed (or joystick equivelent), go to the previous menuItem in list (Previous Screen) */
	else if (nLCDButtons == LCDLeftButton || vexRT[BTN_JOY_LCD_PREVIOUS] == 1)
	{
		waitForLCDButtonRelease();
		LCDPreviousScreen();
	}
	/* If the right LCD Button is pressed (or joystick equivelent), go to the next menuItem in the list (Next screen) */
	else if (nLCDButtons == LCDRightButton || vexRT[BTN_JOY_LCD_NEXT] == 1)
	{
		waitForLCDButtonRelease();
		LCDNextScreen();
	}
	/* If the center LCD Button is pressed (or joystick equivelent)... */
	else if ((nLCDButtons == LCDCenterButton || vexRT[BTN_JOY_LCD_SELECT] == 1) && currentMenu[LCDScreen].id != menuItemBatteryLevel.id && currentMenu[LCDScreen].id != menuItemCurrentProgram.id)
	{
		waitForLCDButtonRelease();
		/* If LCD is on the Autonomous menu screen, go to the direction selection screen */
		if (currentMenu == menuListAuton && currentMenu[LCDScreen].id != menuItemAutonGoBack.id && currentMenu[LCDScreen].isDirectional)
		{
			selectedProgram = &currentMenu[LCDScreen];
			reconfigureMenu(menuListSides, 1, 0, MENU_LIST_SIDES_LENGTH - 1);
		}
		else if (currentMenu == menuListSides && currentMenu[LCDScreen].id != menuItemSideGoBack.id)
		{
			autonomousSide = (currentMenu[LCDScreen].id == menuItemSideLeft.id) ? SIDE_LEFT : SIDE_RIGHT;
			reconfigureMenu(menuListColors, 1, 0, MENU_LIST_COLORS_LENGTH - 1);
		}
		/* If the current menu Item is redirecting to the autonomous screen, go to the menu screen */
		else if (currentMenu[LCDScreen].id == menuItemSideGoBack.id || currentMenu[LCDScreen].id == menuItemGoToAuton.id)
		{
			reconfigureMenu(menuListAuton, 1, 0, MENU_LIST_AUTON_LENGTH - 1);
		}
		else if (currentMenu[LCDScreen].id == menuItemColorGoBack.id)
		{
			reconfigureMenu(menuListSides, 1, 0, MENU_LIST_SIDES_LENGTH - 1);
		}
		/* If current screen is the menu Item for going back to main screen... */
		else if (currentMenu[LCDScreen].id == menuItemAutonGoBack.id)
		{
			LCDScreenMax = MENU_LIST_MAIN_LENGTH - 1;
			/* If in hardcoded competition mode, allow competition mode switch option */
			currentMenu = menuListMain;
			if (isCompetitionMode)
			{
#ifdef competitionEnabled
				LCDScreenMin = menuItemGoToAuton.idx;
#else
				LCDScreenMin = menuItemSwitchCompetitionMode.idx;
#endif
			}
			else LCDScreenMin = menuItemUserControl.idx;
			LCDScreen = menuItemGoToAuton.idx;
		}
		/* If the current screen is the reset gyro screen, reset the gyro and notify the user */
		else if (currentMenu[LCDScreen].id == menuItemResetGyro.id)
		{
			SensorValue[gyro] = 0;
			displayLCDCenteredString(0, "Gyro Reset!");
			clearLCDLine(1);
			wait1Msec(1000);
		}
		/* If the current screen is the switch mode screen... */
		else if (currentMenu[LCDScreen].id == menuItemSwitchCompetitionMode.id)
		{
			isCompetitionMode = !isCompetitionMode;
			if (!isCompetitionMode) LCDAction = "Start";
			else LCDAction = "Select";
			displayLCDCenteredString(0, "Mode Switched!");
			clearLCDLine(1);
			wait1Msec(1000);

			/* If it is currently in competition mode, make usercontrol and PID mode unavailable.
			If hardcoded competition mode, don't show the switch comeptition mode option. */
			if (isCompetitionMode)
			{
				currentMenu = menuListMain;
#ifdef competitionEnabled
				LCDScreenMin = menuItemGoToAuton.idx;
#else
				LCDScreenMin = menuItemSwitchCompetitionMode.idx;
#endif
			}
			else
			{
				LCDScreenMin = menuItemUserControl.idx;
				stopTasks();
				startUp();
			}
		}
		else
		{
			/* If a direction menu item is pressed, update the direction variable. */
			if (currentMenu[LCDScreen].id == menuItemColorBlue.id) autonomousColor = COLOR_BLUE;
			else if (currentMenu[LCDScreen].id == menuItemColorRed.id) autonomousColor = COLOR_RED;
			else selectedProgram = &currentMenu[LCDScreen];

			LCDScreenMax = MENU_LIST_MAIN_LENGTH - 1;
			LCDScreen = menuItemCurrentProgram.idx;
			currentMenu = menuListMain;

			/* If it is currently in competition mode, make usercontrol and PID mode unavailable.
			If hardcoded competition mode, don't show the switch comeptition mode option. */
			if (isCompetitionMode)
			{
#ifdef competitionEnabled
				LCDScreenMin = menuItemGoToAuton.idx;
#else
				LCDScreenMin = menuItemSwitchCompetitionMode.idx;
#endif
			}
			else
			{
				LCDScreenMin = menuItemUserControl.idx;
				stopTasks();
				startUp();
			}
		}
	}
}


task loadLCDScreen() // Task responsible for keeping LCD Screen running.
{
	while (true)
	{
		displayProgram();
		wait1Msec(20);
	}
}



void ResetAllSensors()
{
	int sensorPorts[] = { in1, in2, in3, in4, in5, in6, in7, in8, dgtl1,
		dgtl2, dgtl3, dgtl4, dgtl5, dgtl6, dgtl7, dgtl8,
		dgtl9, dgtl10, dgtl11, dgtl12 };
	for (int i = 0; i < 20; i++)
	{
		SensorValue[sensorPorts[i]] = 0;
	}
}







void pre_auton()
{
	bStopTasksBetweenModes = true; // Extremely glitchy when disabled
	populateMenuItems(); // Update menuItem arrays
	stopTasks(); // Stop all non-main tasks

	/* enable competition mode by default if flag is set */
#ifdef competitionEnabled
	isCompetitionMode = true;
#endif

	if (isLCDEnabled)
	{
		selectedProgram = NULL;
		/* If not hardcoded competition mode, allow User Control option */
#ifndef competitionEnabled
		LCDScreenMin = menuItemUserControl.id;
#else
		LCDScreenMin = menuItemGoToAuton.id;
#endif

		LCDScreenMax = menuItemBatteryLevel.id;
		bLCDBacklight = true; // Turn on LCD Backlight

		/* If not competition mode, set action string to "Start", stop all tasks, and restart LCD Screen */
		if (!isCompetitionMode)
		{
			LCDAction = "<     Start    >";
			selectedProgram = startupLCDProgram;
			LCDScreen = menuItemUserControl.id;
			stopTasks();
			startUp();
		}
		else
		{
			LCDAction = "<    Select    >";
			LCDScreen = menuItemGoToAuton.id;
		}

		displayLCDCenteredString(0, "Daud Jaan");
		displayLCDCenteredString(1, "99 Triple X");
		wait1Msec(1500);

		ResetAllSensors();
		stopTask(loadLCDScreen);
		startTask(loadLCDScreen);

		SensorValue[gyro] = sensorNone;
		wait1Msec(1000);
		SensorValue[gyro] = sensorGyro;
		wait1Msec(2000);

	}
}







/* Start up program based on currently selected program and direction */
void startUp()
{
	stopTasks();
	allMotorsOff();

	string programName = (*selectedProgram).name;
	if ((*selectedProgram).isDirectional)
	{
		if (autonomousSide == SIDE_LEFT) programName += "L";
		else if (autonomousSide == SIDE_RIGHT) programName += "R";

		if (autonomousColor == COLOR_BLUE) programName += "B";
		else if (autonomousColor == COLOR_RED) programName += "R";
	}

	if ((*selectedProgram).id == menuItemUserControl.id) startTask(usercontrol);
	else if ((*selectedProgram).idx < MENU_LIST_AUTON_LENGTH && (*menuListAuton[(*selectedProgram).idx]).id == (*selectedProgram).id)
	{
		/* Countdown if autonomous is selected */
		displayLCDCenteredString(0, programName);
		clearLCDLine(1);
		displayLCDString(1, 2, "Starts In 3");
		wait1Msec(1000);
		displayLCDString(1, 12, "2");
		wait1Msec(1000);
		displayLCDString(1, 12, "1");
		wait1Msec(1000);

		startTask(autonomous);
	}
}







task playMissionImpossibleMusic()
{
	//        100 = Tempo
	//          6 = Default octave
	//    Quarter = Default note length
	//        10% = Break between notes
	//
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(933, 7); wait1Msec(75);  // Note(D#, Duration(32th))
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(933, 7); wait1Msec(75);  // Note(D#, Duration(32th))
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(933, 7); wait1Msec(75);  // Note(D#, Duration(32th))
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(933, 7); wait1Msec(75);  // Note(D#, Duration(32th))
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(880, 7); wait1Msec(75);  // Note(D, Duration(32th))
	playTone(933, 7); wait1Msec(75);  // Note(D#, Duration(32th))
	playTone(988, 7); wait1Msec(75);  // Note(E, Duration(32th))
	playTone(1047, 7); wait1Msec(75);  // Note(F, Duration(32th))
	playTone(1109, 7); wait1Msec(75);  // Note(F#, Duration(32th))
	playTone(1175, 7); wait1Msec(75);  // Note(G, Duration(32th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1398, 14); wait1Msec(150);  // Note(A#, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(784, 14); wait1Msec(150);  // Note(C, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1047, 14); wait1Msec(150);  // Note(F, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1109, 14); wait1Msec(150);  // Note(F#, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1398, 14); wait1Msec(150);  // Note(A#, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(784, 14); wait1Msec(150);  // Note(C, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(0, 27); wait1Msec(300);  // Note(Rest, Duration(Eighth))
	playTone(1047, 14); wait1Msec(150);  // Note(F, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1109, 14); wait1Msec(150);  // Note(F#, Duration(16th))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(1398, 14); wait1Msec(150);  // Note(A#, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(880, 108); wait1Msec(1200);  // Note(D, Duration(Half))
	playTone(0, 7); wait1Msec(75);  // Note(Rest, Duration(32th))
	playTone(1398, 14); wait1Msec(150);  // Note(A#, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(831, 108); wait1Msec(1200);  // Note(C#, Duration(Half))
	playTone(0, 7); wait1Msec(75);  // Note(Rest, Duration(32th))
	playTone(1398, 14); wait1Msec(150);  // Note(A#, Duration(16th))
	playTone(1175, 14); wait1Msec(150);  // Note(G, Duration(16th))
	playTone(784, 108); wait1Msec(1200);  // Note(C, Duration(Half))
	playTone(0, 14); wait1Msec(150);  // Note(Rest, Duration(16th))
	playTone(932, 14); wait1Msec(150);  // Note(A#5, Duration(16th))
	playTone(784, 14); wait1Msec(150);  // Note(C, Duration(16th))

}


short getGyroSensorValue() {
	return SensorValue[gyro];
}


/*________________GLOBAL VARIABLES START________________*/
//ball behind indexer
bool ballstatus = false;
//autoIntake
bool intakeSystem = false;
bool outtakeSystem = false;

//BANG BANG + PID
int RPM;
int currentGoalVelocity;
int hi; // PRABHS CANCER FIX LATER
int motorpower = 0;


int target = 0;
int current = 0;
int error = 0;
int time = 0;
int rate = 0;
int errorSum = 0;
int lastError = 0;
int errorDifference = 0;


int pid_tol=100;
float ki =  0.001;
float kp= 0.1;
float kd = -0.1;

int direction;
bool killflywheel=false;
bool idler = true;
int idlepower = 47;

//flywheel RPM
bool FlywheelState;
//autoShoot
bool runFlywheel=false;
bool rpmReady = false;
//arm up
int targetheight = 60;
int errorarm = targetheight-SensorValue[armenc];
float armkp=0.2;

/*________________GLOBAL VARIABLES END________________*/

//ball behind indexer
task checkball() {
	while (true) {
		//if ball behind sensor, set status of ball
		if(SensorValue[indexbtn]==1) {
			ballstatus=true;
		}
		else {
			ballstatus=false;
		}
		wait1Msec(20);
	}
}

//autoIntake
task autointake()
{
	while (true) {
		//intake balls
		while (intakeSystem==true) {
			//for intaking (not shooting
			if (runFlywheel==false) {
				//no ball behind indexer
				if (ballstatus==false) {
					//run both indexer and intake
					motor[indexer]=0;
					motor[intake]=-127;
				}
				//ball behind indexer
				else if (ballstatus==true) {
					//turn off indexer and only run intake
					motor[indexer]=0;
					motor[intake]=-127;
				}
			}
			//intake operation while runningflywheel
			if (runFlywheel==true) {
				//When the rpm is correct
				if(rpmReady==true) {
					//and there is no ball behind the indexer
					if(ballstatus==false) {
						//run both indexer and intake
						motor[indexer]=-127;
						motor[intake]=-60;
					}
					//or there is a ball behind the indexer
					else if (ballstatus==true) {
						//turn off intake and only run indexer
						motor[indexer]=-127;
						motor[intake]=0;
					}
				}
				//When rpm is not close to target
				else if (rpmReady==false) {
					//and there is no ball behind the indexer
					if (ballstatus==false) {
						//run both indexer and intake
						motor[indexer]=0;
						motor[intake]=0;
					}
					//or there is a ball behind the indexer
					else if (ballstatus==true) {
						//turn off both motors
						motor[indexer]=0;
						motor[intake]=0;
					}

				}
			}
			wait1Msec(20);
		}
		//do not intake balls
		while (intakeSystem==false)
		{
			if(outtakeSystem==true) {
				motor[indexer]=127;
				motor[intake]=127;
				} else {
				motor[indexer]=0;
				motor[intake]=0;
			}
			wait1Msec(20);
		}
	}
}




float maxIntegral = 80.0;
const int errorArrayLength = 5;
int errors[errorArrayLength];
int lastErrors[errorArrayLength];
byte errorIndex = 0;

//BANG BANG + PID

void copyArray(int *arr1, int *arr2, int size) {
	for (int i = 0; i < size; i++) {
		arr1[i] = arr2[i];
	}
}

float avgArray(int *array, int size) {
	float avg = 0;
	for (int i = 0; i < size; i++) {
		avg += array[i];
	}
	return avg / (float) size;
}


task Flywheel_Control()
{
	while (true) {
		while (idler==false) {
			target=currentGoalVelocity;
			current=RPM;
			lastError = error;
			error=target-current;

			if (errorIndex == errorArrayLength - 1) {
				copyArray(lastErrors, errors, errorArrayLength);
			}

			errors[errorIndex] = error;

			if (errorIndex < errorArrayLength - 1) errorIndex++;
			else errorIndex = 0;

			error = avgArray(errors, errorArrayLength);
			lastError = avgArray(lastErrors, errorArrayLength);

			if (abs(error) > pid_tol) {
				if (abs(current) < target) {
					motorpower = 127;
					} else if(abs(current) > target) {
					motorpower = 40;
				}
			}

			if(abs(error) < pid_tol) {
				errorSum += error;
				errorDifference = error - lastError;

				if (errorSum * kd > maxIntegral) errorSum = maxIntegral / kd;
				else if (errorSum * kd < -maxIntegral) errorSum = -maxIntegral / kd;

				motorpower = 68 + (error * kp + errorSum * ki + errorDifference * kd);
			}

			wait1Msec(40);
		}
		while(idler == true)
		{
			if (RPM < 1600) {
				motorpower=abs(127);
				} else {
				motorpower=abs(idlepower);
			}
			if (killflywheel==true) {
				motorpower=0;
			}

			wait1Msec(20);
		}
		wait1Msec(20);
	}
}


//set flywheelpower
task flywheelpower ()
{
	while(true)
	{
		if(motorpower < 0)
		{
			motorpower = 0;
		}
		motor[flywheel1] = motorpower;
		motor[flywheel2] = motorpower;

		wait1Msec(20);
	}
}

//flywheel RPM
task GetRPM ()
{
	int LastValue;
	int CurrentValue;
	int Error;
	SensorValue(armenc) = 0;

	while(true){
		if(FlywheelState == true){
			LastValue = SensorValue(flywheelenc);
			wait1Msec(20);
			CurrentValue = SensorValue(flywheelenc);
			Error = CurrentValue - LastValue;
			SensorValue(flywheelenc) = 0;
			RPM = (Error*15000)/359;
		}
		else if (FlywheelState == false){
			SensorValue(flywheelenc) = 0;
			RPM = 0;
			Error = 0;

		}
		hi = SensorValue(armenc);
		wait1Msec(20);
	}

}

//autoShoot
task autoShoot () {
	while (true) {
		//when you want to shoot
		while(runFlywheel==true) {
			//turn off idle to run PID + bang bang
			idler = false;
			//run autointake
			intakeSystem=true;
			//when rpm is correct
			if ((abs(error)<50)) {
				rpmReady=true;
			}
			//if rpm is not correct
			else if (abs(error)>50) {
				rpmReady=false;
			}
			wait1Msec(20);
		}
		//when you are not shooting return to idle
		while(runFlywheel==false) {
			idler=true;
			wait1Msec(20);
		}
		wait1Msec(20);
	}
}

//arm up
task armauton2()
{
	motor[arm_cap1]=errorarm*armkp-38;
	motor[arm_cap2]=errorarm*armkp-38;
	delay(20);
}
// PID MOVEMENTS CODE STARTS HERE
void positiveTurn(int angle, int speed, int max_time)
{
	int left = 0, right = 0;
	SensorValue[gyro] = 0;
	if (angle < 0){left = -1; right = 1;}
	else if (angle > 0){left = 1; right = -1; }
	clearTimer(T3);

	while(abs(SensorValue[gyro]) < angle)
	{
		if(time1[T3] > max_time)
		{
			break;
		}
		motor[drive_l1] = speed*left;
		motor[drive_r1] = speed*right;
		motor[drive_l2] = speed*left;
		motor[drive_r2] = speed*right;
		wait1Msec(20);
	}
	motor[drive_l1] = (speed*left*-1)/9;
	motor[drive_l2] = (speed*left*-1)/9;
	motor[drive_r1] = (speed*right*-1)/9;
	motor[drive_r2] = (speed*right*-1)/9;

	wait1Msec(250);
	motor[drive_l1] = 0;
	motor[drive_l2] = 0;
	motor[drive_r1] = 0;
	motor[drive_r2] = 0;

}

void negativeTurn(int angle, int speed, int max_time)
{
	int left = 0, right = 0;
	SensorValue[gyro] = 0;
	if (angle < 0){left = 1; right = -1;}
	else if (angle > 0){left = -1; right = 1; }
	clearTimer(T3);

	while(abs(SensorValue[gyro]) < angle)
	{
		if(time1[T3] > max_time)
		{
			break;
		}
		motor[drive_l1] = speed*left;
		motor[drive_r1] = speed*right;
		motor[drive_l2] = speed*left;
		motor[drive_r2] = speed*right;
		wait1Msec(20);
	}
	motor[drive_l1] = (speed*left*-1)/9;
	motor[drive_l2] = (speed*left*-1)/9;
	motor[drive_r1] = (speed*right*-1)/9;
	motor[drive_r2] = (speed*right*-1)/9;

	wait1Msec(250);
	motor[drive_l1] = 0;
	motor[drive_l2] = 0;
	motor[drive_r1] = 0;
	motor[drive_r2] = 0;

}

void positivedistance(int distance, int speed, int max_time)
{
	SensorValue[leftdrive_enc] = 0;
	SensorValue[rightdrive_enc] = 0;
	int direction;
	if (distance < 0){direction = -1;}
	else if (distance > 0){direction = 1;}
	SensorValue[gyro] = 0;
	clearTimer(T3);
	if(abs(0 - SensorValue[gyro])<20)
	{
		while(abs(SensorValue[leftdrive_enc]) < distance*direction && abs(SensorValue[rightdrive_enc]) < distance*direction)
		{
			if(time1[T3] > max_time)
			{
				break;
			}
			motor[drive_l1] = speed*direction;
			motor[drive_r1] = speed*direction;
			motor[drive_l2] = speed*direction;
			motor[drive_r2] = speed*direction;
			wait1Msec(20);
		}
		motor[drive_l1] = (speed*-direction)/9;
		motor[drive_r1] = (speed*-direction)/9;
		motor[drive_l2] = (speed*-direction)/9;
		motor[drive_r2] = (speed*-direction)/9;
		wait1Msec(250);
		motor[drive_l1] = 0;
		motor[drive_r1] = 0;
		motor[drive_l2] = 0;
		motor[drive_r2] = 0;
		return;
	}
	else
	{
		positiveTurn((0 - SensorValue[gyro]), 127, 200);
	}


}

void negativedistance(int distance, int speed, int max_time)
{
	SensorValue[leftdrive_enc] = 0;
	SensorValue[rightdrive_enc] = 0;
	int direction;
	if (distance < 0){direction = -1;}
	else if (distance > 0){direction = 1;}
	SensorValue[gyro] = 0;
	clearTimer(T3);
	if(abs(0 - SensorValue[gyro])<20)
	{
		while(SensorValue[rightdrive_enc] < distance*direction)
		{
			if(time1[T3] > max_time)
			{
				break;
			}
			motor[drive_l1] = speed*-direction;
			motor[drive_r1] = speed*-direction;
			motor[drive_l2] = speed*-direction;
			motor[drive_r2] = speed*-direction;
			wait1Msec(20);
		}
		motor[drive_l1] = (speed*-direction)/9;
		motor[drive_r1] = (speed*-direction)/9;
		motor[drive_l2] = (speed*-direction)/9;
		motor[drive_r2] = (speed*-direction)/9;
		wait1Msec(250);
		motor[drive_l1] = 0;
		motor[drive_r1] = 0;
		motor[drive_l2] = 0;
		motor[drive_r2] = 0;
		return;
	}
	else
	{
		negativeTurn((0 - SensorValue[gyro]), 127, 200);
	}


}


void pre_auton()
{

}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                              Autonomous Task                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/

task autonomous()
{
	/*---------------------------------------------------------------------------*/
	/*                                                                           */
	/*                              Red Close 3 Shoot                              */
	/*                                                                           */
	/*---------------------------------------------------------------------------*/

	if ((*selectedProgram) == menuItemAutonFront && (autonomousSide == COLOR_RED)){

		startTask(GetRPM); //Calculates RPM of flywheel
		FlywheelState=true;
		currentGoalVelocity = 2400; //Assigns desired RPM
		startTask(flywheelpower); //Assign flywheel motors and safe proof the motors
		startTask(Flywheel_Control); //Run the flywheel
		//startTask(autoShoot);
		idler=false;
		motor[intake]=-127; //Runs the intake Starts
		positivedistance(650,80,3000); //Goes towards the ball to intake
		motor[arm_cap1]=-30; //Arm is expanded starts
		motor[arm_cap2]=-30;
		wait1Msec(200);
		motor[arm_cap1]=30;
		motor[arm_cap2]=30;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm is expanded ends
		motor[intake]=0; //Intake stops after the ball is in
		negativedistance(300,80,1700); //Reverses and realigns with wall
		positivedistance(20,50,600); //Drives slight forward away from the wall
		negativeTurn(400,50,1000); //Turns towards wall 1
		negativeTurn(280,35,800); //Turns towards wall 2
		positivedistance(150,50,1200); //Moves forwards CHANGED
		positiveTurn(40,50,500); //Turns slightly away from the wall
		positivedistance(30,50,600); //Moves closer to flags with the same angle
		motor[indexer]=-127; //Runs the indexer for the first ball = high flag
		wait1Msec(300);
		motor[indexer]=0; //Stops indexer
		positivedistance(120,50,1600); //Drive forward towards flags
		wait1Msec(200); //Small pause
		motor[indexer]=-127; //Runs the indexer
		motor[intake]=-127; //Runs the intake
		wait1Msec(600); //Second ball shoot=middle flag
		motor[indexer]=0; //Stop indexer
		motor[intake]=0; //Stop intake
		wait1Msec(300);
		negativedistance(15,50,300); //Reverse before last turn
		negativeTurn(25,40,500); //Turns slightly towards the wall
		positivedistance(500,75,1400); //Drives forward and realigns
		negativedistance(70,70,700); //Reverse to match distance with cap
		motor[intake]=127; //Starts the intake to flip cap
		positiveTurn(755,50,2500); //Turns so the intake faces the cap
		positivedistance(400,127,3000); //Drives towards cap and flips it CHANGED

	}

	/*---------------------------------------------------------------------------*/
	/*                                                                           */
	/*                              Far BLUE Cap                           */
	/*                                                                           */
	/*---------------------------------------------------------------------------*/


	if ((*selectedProgram) == menuItemAutonBack && (autonomousSide == COLOR_RED)){
		startTask(GetRPM); //Calculates RPM of flywheel
		FlywheelState=true;
		currentGoalVelocity = 2400; //Assigns desired RPM
		startTask(flywheelpower); //Assign flywheel motors and safe proof the motors
		startTask(Flywheel_Control); //Run the flywheel
		idler=true;
		motor[intake]=-127; //Intake turns on (Take in ball)
		positivedistance(600,80,2000); // Drive towards the ball
		motor[arm_cap1]=-30;	//Arm expand starts
		motor[arm_cap2]=-30;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0;		//Arm expand ends
		motor[intake]=0; //Intake stops after ball is in indexer
		negativedistance(85,60,550);	//Reverse with ball
		positiveTurn(1150,50,2500); //Turns so the arm claw is alligned with cap
		motor[arm_cap1]=50;	//Arm is pushed down to the ground
		motor[arm_cap2]=50;
		negativedistance(5,60,500); //Reverse to slide cap in arm claw
		negativedistance(8,40,250); //Reverse to slide cap in arm claw
		startTask(armauton2); //PID to keep the cap lifted
		wait1Msec(500);
		positivedistance(80,50,800); //Drives forward with cap
		positiveTurn(295,50,1300); //Turns toward the pole
		positivedistance(550,80,2200); //Drive upto the pole
		motor[arm_cap1]=-127; //Arm travels to smash the cap on pole starts
		motor[arm_cap2]=-127;
		wait1Msec(1200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm travels to smash the cap on pole ends
		negativedistance(200,60,1000); //Reverse with same arm length
		motor[arm_cap1]=127; //Arm goes down to starting position starts
		motor[arm_cap2]=127;
		wait1Msec(800);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm goes down to starting position starts
		positiveTurn(720,80,2200); //Turns to platform
		motor[arm_cap1]=-80; //Arm lifts slighlty to go up platform start
		motor[arm_cap2]=-80;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm lifts slighlty to go up platform ends
		negativedistance(20,80,300); //Drives upto the platform edge
		motor[drive_l1]=-127; //Drives up platform at full speed starts
		motor[drive_l2]=-127;
		motor[drive_r1]=-127;
		motor[drive_r2]=-127;
		wait1Msec(900);
		motor[drive_l1]=0;
		motor[drive_l2]=0;
		motor[drive_r1]=0;
		motor[drive_r2]=0; //Drives up platform at full speed ends



	}


	/*---------------------------------------------------------------------------*/
	/*                                                                           */
	/*                              Blue Close 3 Shoot                              */
	/*                                                                           */
	/*---------------------------------------------------------------------------*/

	if ((*selectedProgram) == menuItemAutonFront && (autonomousSide == COLOR_BLUE)){

		startTask(GetRPM); //Calculates RPM of flywheel
		FlywheelState=true;
		currentGoalVelocity = 2400; //Assigns desired RPM
		startTask(flywheelpower); //Assign flywheel motors and safe proof the motors
		startTask(Flywheel_Control); //Run the flywheel
		//startTask(autoShoot);
		idler=false;
		motor[intake]=-127; //Runs the intake Starts
		positivedistance(650,80,3000); //Goes towards the ball to intake
		motor[arm_cap1]=-30; //Arm is expanded starts
		motor[arm_cap2]=-30;
		wait1Msec(200);
		motor[arm_cap1]=30;
		motor[arm_cap2]=30;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm is expanded ends
		motor[intake]=0; //Intake stops after the ball is in
		negativedistance(300,80,1700); //Reverses and realigns with wall
		positivedistance(20,50,600); //Drives slight forward away from the wall
		positiveTurn(400,50,1000); //Turns towards wall 1
		positiveTurn(400,35,1200); //Turns towards wall 2
		positivedistance(150,50,1200); //Moves forwards CHANGED
		negativeTurn(80,50,850); //Turns slightly away from the wall
		positivedistance(30,50,600); //Moves closer to flags with the same angle
		motor[indexer]=-127; //Runs the indexer for the first ball = high flag
		wait1Msec(300);
		motor[indexer]=0; //Stops indexer
		positivedistance(120,50,1600); //Drive forward towards flags
		wait1Msec(200); //Small pause
		motor[indexer]=-127; //Runs the indexer
		motor[intake]=-127; //Runs the intake
		wait1Msec(600); //Second ball shoot=middle flag
		motor[indexer]=0; //Stop indexer
		motor[intake]=0; //Stop intake
		wait1Msec(300);
		negativedistance(15,50,300); //Reverse before last turn
		positiveTurn(35,50,580); //Turns slightly towards the wall
		positivedistance(500,75,1400); //Drives forward and realigns
		negativedistance(60,60,600); //Reverse to match distance with cap
		motor[intake]=127; //Starts the intake to flip cap
		negativeTurn(755,50,2500); //Turns so the intake faces the cap
		positivedistance(400,127,3000); //Drives towards cap and flips it CHANGED
	}


	/*---------------------------------------------------------------------------*/
	/*                                                                           */
	/*                              Far RED Cap                           */
	/*                                                                           */
	/*---------------------------------------------------------------------------*/

	if ((*selectedProgram) == menuItemAutonBack && (autonomousSide == COLOR_BLUE)){
		startTask(GetRPM); //Calculates RPM of flywheel
		FlywheelState=true;
		currentGoalVelocity = 2400; //Assigns desired RPM
		startTask(flywheelpower); //Assign flywheel motors and safe proof the motors
		startTask(Flywheel_Control); //Run the flywheel
		idler=true;
		motor[intake]=-127; //Intake turns on (Take in ball)
		positivedistance(600,80,2000); // Drive towards the ball
		motor[arm_cap1]=-30;	//Arm expand starts
		motor[arm_cap2]=-30;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0;		//Arm expand ends
		motor[intake]=0; //Intake stops after ball is in indexer
		negativedistance(85,60,550);	//Reverse with ball
		negativeTurn(1150,50,2500); //Turns so the arm claw is alligned with cap
		motor[arm_cap1]=50;	//Arm is pushed down to the ground
		motor[arm_cap2]=50;
		negativedistance(5,60,500); //Reverse to slide cap in arm claw
		negativedistance(8,40,250); //Reverse to slide cap in arm claw
		startTask(armauton2); //PID to keep the cap lifted
		wait1Msec(500);
		positivedistance(30,50,400); //Drives forward with cap
		negativeTurn(250,50,1000); //Turns toward the pole
		positivedistance(550,80,2200); //Drive upto the pole
		motor[arm_cap1]=-127; //Arm travels to smash the cap on pole starts
		motor[arm_cap2]=-127;
		wait1Msec(1200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm travels to smash the cap on pole ends
		negativedistance(200,60,1000); //Reverse with same arm length
		motor[arm_cap1]=127; //Arm goes down to starting position starts
		motor[arm_cap2]=127;
		wait1Msec(800);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm goes down to starting position starts
		negativeTurn(720,80,2200); //Turns to platform
		motor[arm_cap1]=-80; //Arm lifts slighlty to go up platform start
		motor[arm_cap2]=-80;
		wait1Msec(200);
		motor[arm_cap1]=0;
		motor[arm_cap2]=0; //Arm lifts slighlty to go up platform ends
		negativedistance(20,80,300); //Drives upto the platform edge
		motor[drive_l1]=-127; //Drives up platform at full speed starts
		motor[drive_l2]=-127;
		motor[drive_r1]=-127;
		motor[drive_r2]=-127;
		wait1Msec(900);
		motor[drive_l1]=0;
		motor[drive_l2]=0;
		motor[drive_r1]=0;
		motor[drive_r2]=0; //Drives up platform at full speed ends

	}
}



/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                              User Control Task                            */
/*                                                                           */
/*  This task is used to control your robot during the user control phase of */
/*  a VEX Competition.                                                       */
/*                                                                           */
/*  You must modify the code to add your own robot specific commands here.   */
/*---------------------------------------------------------------------------*/
float error_arm = -127;

float targetarm = -15;
bool armpreset;


task usercontrol()
{
	startTask (GetRPM);
	FlywheelState = true;
	startTask (flywheelpower);
	startTask (Flywheel_Control);
	startTask (checkball);
	startTask(autoShoot);
	startTask(autointake);


	int buttonToggleState = 0;
	int buttonPressed = 0;

	while (true)
	{
		if( vexRT[Btn7U] == 1 && vexRT[Btn5U]== 0 && vexRT[Btn5D]==0 )
		{
			if( ! buttonPressed )
			{
				// change the toggle state
				buttonToggleState = 1 - buttonToggleState;

				// Note the button is pressed
				buttonPressed = 1;
			}
		}
		else
		{
			// the button is not pressed
			buttonPressed = 0;
		}
		// Now do something with our toggle flag
		if( buttonToggleState )
		{
			killflywheel=true;
			targetarm=10;
			armpreset=true;
		}
		else
		{
			killflywheel=false;
			targetarm=-15;
		}


		if (vexRT[Btn7L]==1)
		{
			motor[drive_l1]  = -127;
			motor[drive_l2]  = -127;
			motor[drive_r1] =  -127;
			motor[drive_r2] = -127;
		}
		else
		{
			motor[drive_l1]  = (vexRT[Ch2] + vexRT[Ch1])/2;
			motor[drive_l2]  = (vexRT[Ch2] + vexRT[Ch1])/2;
			motor[drive_r1] =  (vexRT[Ch2] - vexRT[Ch1])/2;
			motor[drive_r2] = (vexRT[Ch2] - vexRT[Ch1])/2;

		}

		/*-------killflywheel off---------------*/
		//apply brake
		if (vexRT[Btn5U]==true && vexRT[Btn5D]==true) {
			motor[arm_cap1]=50;
			motor[arm_cap2]=50;
			wait1Msec(700);
			motor[arm_cap1]=0;
			motor[arm_cap2]=0;

			if(RPM < 50) {
				stopTask (flywheelpower);
				motor[flywheel1] = -100;
				motor[flywheel2] = -100;
				wait1Msec(12000);

				while(true){
					motor[drive_l1]=-20;
					motor[drive_l2]=20;
					motor[drive_r1]=-20;
					motor[drive_r2]=20;

				}

			}

		}




		//THIS IS THE ARM PRESET HEIGHT
		if(vexRT[Btn5D]==1 && vexRT[Btn7U]==false && vexRT[Btn5U]==0)
		{
			armpreset=true;
		}
		else if (vexRT[Btn5D]==0 && vexRT[Btn7U]==false && buttonToggleState==0)
		{
			armpreset=false;
		}

		if (armpreset==true)
		{
			if (hi >= targetarm)
			{
				motor[arm_cap1]=error_arm;
				motor[arm_cap2]=error_arm;
				delay(20);
			}

			else if(hi <= targetarm) {
				motor[arm_cap1] = -15;
				motor[arm_cap2] = -15;
			}
		}

		if (armpreset==false)
		{
			// THIS IS THE ARM CONTROL

			motor[arm_cap1] = (vexRT[Ch4] - vexRT[Ch3])/2;
			motor[arm_cap2] = (vexRT[Ch4] - vexRT[Ch3])/2;
		}
		//PRESET FOR HIGH RPM (3000)
		if (vexRT[Btn6D]==1 && vexRT[Btn6U]==0)
		{
			currentGoalVelocity=2400;
			runFlywheel=true;
		}
		else if (vexRT[Btn6D]==0 && vexRT[Btn6U]==0)
		{
			runFlywheel=false;
		}
		//intake
		if(vexRT[Btn6U]==1 && vexRT[Btn6D]==0)
		{
			intakeSystem=true;
		}
		else if (vexRT[Btn6U]==0 && vexRT[Btn6D]==0)
		{
			intakeSystem=false;
		}
		//outtake
		if(vexRT[Btn5U]==1 && vexRT[Btn7U]==false && vexRT[Btn5D]==false) {
			outtakeSystem=true;
		}
		else {
			outtakeSystem=false;
		}

		if (vexRT[Btn8U]==1)
		{
			negativeTurn(30,50,500);
		}
		else if (vexRT[Btn8D]==1)
		{
			positiveTurn(30,50,500);
		}

		wait1Msec(20);
	}

}

/* Stop all non-vital tasks */
void stopTasks()
{
	stopTask(autonomous);
	stopTask(usercontrol);
	stopTask(playMissionImpossibleMusic);
	stopTask(GetRPM);
	stopTask(flywheelpower);
	stopTask(Flywheel_Control);
	stopTask(checkball);
	stopTask(autointake);
	stopTask(autoShoot);
	stopTask(armauton2);
}
