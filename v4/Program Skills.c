#pragma config(Sensor, in4,    lightindexer,   sensorReflection)
#pragma config(Sensor, in5,    potarm1,        sensorPotentiometer)
#pragma config(Sensor, in6,    gyro,           sensorGyro)
#pragma config(Sensor, dgtl1,  armenc,         sensorQuadEncoder)
#pragma config(Sensor, dgtl4,  indexbtn,       sensorTouch)
#pragma config(Sensor, dgtl5,  rightdrive_enc, sensorQuadEncoder)
#pragma config(Sensor, dgtl7,  leftdrive_enc,  sensorQuadEncoder)
#pragma config(Sensor, dgtl11, flywheelenc,    sensorQuadEncoder)
#pragma config(Motor,  port1,           indexer,       tmotorVex393_HBridge, openLoop)
#pragma config(Motor,  port2,           drive_r1,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port3,           flywheel1,     tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port4,           drive_r2,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port5,           arm_cap1,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port6,           arm_cap2,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port7,           drive_l2,      tmotorVex393_MC29, openLoop, reversed)
#pragma config(Motor,  port8,           drive_l1,      tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port9,           flywheel2,     tmotorVex393_MC29, openLoop)
#pragma config(Motor,  port10,          intake, tmotorVex393_HBridge, openLoop)
#pragma config(Sensor, in3,    potentiometerLCD, sensorPotentiometer)
#pragma config(Sensor, in2,    secondaryBattery, sensorAnalog)

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*        Description: Competition template for VEX EDR                      */
/*                                                                           */
/*---------------------------------------------------------------------------*/

// This code is for the VEX cortex platform
#pragma platform(VEX2)

// Select Download method as "competition"
#pragma competitionControl(Competition)

//Main competition background code...do not modify!
#include "Vex_Competition_Includes.c"



/*________________GLOBAL VARIABLES START________________*/
//ball behind indexer
bool ballstatus = false;
//autoIntake
bool intakeSystem = false;
bool outtakeSystem = false;

//BANG BANG + PID
int RPM;
int currentGoalVelocity;
int hi; // PRABHS CANCER FIX LATER
int motorpower = 0;


int target = 0;
int current = 0;
int error = 0;
int time = 0;
int rate = 0;
int errorSum = 0;
int lastError = 0;
int errorDifference = 0;


int pid_tol=100;
float ki =  0.001;
float kp= 0.1;
float kd = -0.1;

int direction;
bool killflywheel=false;
bool idler = true;
int idlepower = 47;

//flywheel RPM
bool FlywheelState;
//autoShoot
bool runFlywheel=false;
bool rpmReady = false;
//arm up
int targetheight = 60;
int errorarm = targetheight-SensorValue[armenc];
float armkp=0.2;

/*________________GLOBAL VARIABLES END________________*/

//ball behind indexer
task checkball() {
	while (true) {
		//if ball behind sensor, set status of ball
		if(SensorValue[indexbtn]==1) {
			ballstatus=true;
		}
		else {
			ballstatus=false;
		}
		wait1Msec(20);
	}
}

//autoIntake
task autointake()
{
	while (true) {
		//intake balls
		while (intakeSystem==true) {
			//for intaking (not shooting
			if (runFlywheel==false) {
				//no ball behind indexer
				if (ballstatus==false) {
					//run both indexer and intake
					motor[indexer]=0;
					motor[intake]=-127;
				}
				//ball behind indexer
				else if (ballstatus==true) {
					//turn off indexer and only run intake
					motor[indexer]=0;
					motor[intake]=-127;
				}
			}
			//intake operation while runningflywheel
			if (runFlywheel==true) {
				//When the rpm is correct
				if(rpmReady==true) {
					//and there is no ball behind the indexer
					if(ballstatus==false) {
						//run both indexer and intake
						motor[indexer]=-127;
						motor[intake]=-60;
					}
					//or there is a ball behind the indexer
					else if (ballstatus==true) {
						//turn off intake and only run indexer
						motor[indexer]=-127;
						motor[intake]=0;
					}
				}
				//When rpm is not close to target
				else if (rpmReady==false) {
					//and there is no ball behind the indexer
					if (ballstatus==false) {
						//run both indexer and intake
						motor[indexer]=0;
						motor[intake]=0;
					}
					//or there is a ball behind the indexer
					else if (ballstatus==true) {
						//turn off both motors
						motor[indexer]=0;
						motor[intake]=0;
					}

				}
			}
			wait1Msec(20);
		}
		//do not intake balls
		while (intakeSystem==false)
		{
			if(outtakeSystem==true) {
				motor[indexer]=127;
				motor[intake]=127;
				} else {
				motor[indexer]=0;
				motor[intake]=0;
			}
			wait1Msec(20);
		}
	}
}




float maxIntegral = 80.0;
const int errorArrayLength = 5;
int errors[errorArrayLength];
int lastErrors[errorArrayLength];
byte errorIndex = 0;

//BANG BANG + PID

void copyArray(int *arr1, int *arr2, int size) {
	for (int i = 0; i < size; i++) {
		arr1[i] = arr2[i];
	}
}

float avgArray(int *array, int size) {
	float avg = 0;
	for (int i = 0; i < size; i++) {
		avg += array[i];
	}
	return avg / (float) size;
}


task Flywheel_Control()
{
	while (true) {
		while (idler==false) {
			target=currentGoalVelocity;
			current=RPM;
			lastError = error;
			error=target-current;

			if (errorIndex == errorArrayLength - 1) {
				copyArray(lastErrors, errors, errorArrayLength);
			}

			errors[errorIndex] = error;

			if (errorIndex < errorArrayLength - 1) errorIndex++;
			else errorIndex = 0;

			error = avgArray(errors, errorArrayLength);
			lastError = avgArray(lastErrors, errorArrayLength);

			if (abs(error) > pid_tol) {
				if (abs(current) < target) {
					motorpower = 127;
					} else if(abs(current) > target) {
					motorpower = 40;
				}
			}

			if(abs(error) < pid_tol) {
				errorSum += error;
				errorDifference = error - lastError;

				if (errorSum * kd > maxIntegral) errorSum = maxIntegral / kd;
				else if (errorSum * kd < -maxIntegral) errorSum = -maxIntegral / kd;

				motorpower = 68 + (error * kp + errorSum * ki + errorDifference * kd);
			}

			wait1Msec(40);
		}
		while(idler == true)
		{
			if (RPM < 1600) {
				motorpower=abs(127);
				} else {
				motorpower=abs(idlepower);
			}
			if (killflywheel==true) {
				motorpower=0;
			}

			wait1Msec(20);
		}
		wait1Msec(20);
	}
}


//set flywheelpower
task flywheelpower ()
{
	while(true)
	{
		if(motorpower < 0)
		{
			motorpower = 0;
		}
		motor[flywheel1] = motorpower;
		motor[flywheel2] = motorpower;

		wait1Msec(20);
	}
}

//flywheel RPM
task GetRPM ()
{
	int LastValue;
	int CurrentValue;
	int Error;
	SensorValue(armenc) = 0;

	while(true){
		if(FlywheelState == true){
			LastValue = SensorValue(flywheelenc);
			wait1Msec(20);
			CurrentValue = SensorValue(flywheelenc);
			Error = CurrentValue - LastValue;
			SensorValue(flywheelenc) = 0;
			RPM = (Error*15000)/359;
		}
		else if (FlywheelState == false){
			SensorValue(flywheelenc) = 0;
			RPM = 0;
			Error = 0;

		}
		hi = SensorValue(armenc);
		wait1Msec(20);
	}

}

//autoShoot
task autoShoot () {
	while (true) {
		//when you want to shoot
		while(runFlywheel==true) {
			//turn off idle to run PID + bang bang
			idler = false;
			//run autointake
			intakeSystem=true;
			//when rpm is correct
			if ((abs(error)<50)) {
				rpmReady=true;
			}
			//if rpm is not correct
			else if (abs(error)>50) {
				rpmReady=false;
			}
			wait1Msec(20);
		}
		//when you are not shooting return to idle
		while(runFlywheel==false) {
			idler=true;
			wait1Msec(20);
		}
		wait1Msec(20);
	}
}

//arm up
task armauton2()
{
	motor[arm_cap1]=errorarm*armkp-38;
	motor[arm_cap2]=errorarm*armkp-38;
	delay(20);
}
// PID MOVEMENTS CODE STARTS HERE
void positiveTurn(int angle, int speed, int max_time)
{
	int left = 0, right = 0;
	SensorValue[gyro] = 0;
	if (angle < 0){left = -1; right = 1;}
	else if (angle > 0){left = 1; right = -1; }
	clearTimer(T3);

	while(abs(SensorValue[gyro]) < angle)
	{
		if(time1[T3] > max_time)
		{
			break;
		}
		motor[drive_l1] = speed*left;
		motor[drive_r1] = speed*right;
		motor[drive_l2] = speed*left;
		motor[drive_r2] = speed*right;
		wait1Msec(20);
	}
	motor[drive_l1] = (speed*left*-1)/9;
	motor[drive_l2] = (speed*left*-1)/9;
	motor[drive_r1] = (speed*right*-1)/9;
	motor[drive_r2] = (speed*right*-1)/9;

	wait1Msec(250);
	motor[drive_l1] = 0;
	motor[drive_l2] = 0;
	motor[drive_r1] = 0;
	motor[drive_r2] = 0;

}

void negativeTurn(int angle, int speed, int max_time)
{
	int left = 0, right = 0;
	SensorValue[gyro] = 0;
	if (angle < 0){left = 1; right = -1;}
	else if (angle > 0){left = -1; right = 1; }
	clearTimer(T3);

	while(abs(SensorValue[gyro]) < angle)
	{
		if(time1[T3] > max_time)
		{
			break;
		}
		motor[drive_l1] = speed*left;
		motor[drive_r1] = speed*right;
		motor[drive_l2] = speed*left;
		motor[drive_r2] = speed*right;
		wait1Msec(20);
	}
	motor[drive_l1] = (speed*left*-1)/9;
	motor[drive_l2] = (speed*left*-1)/9;
	motor[drive_r1] = (speed*right*-1)/9;
	motor[drive_r2] = (speed*right*-1)/9;

	wait1Msec(250);
	motor[drive_l1] = 0;
	motor[drive_l2] = 0;
	motor[drive_r1] = 0;
	motor[drive_r2] = 0;

}

void positivedistance(int distance, int speed, int max_time)
{
	SensorValue[leftdrive_enc] = 0;
	SensorValue[rightdrive_enc] = 0;
	int direction;
	if (distance < 0){direction = -1;}
	else if (distance > 0){direction = 1;}
	SensorValue[gyro] = 0;
	clearTimer(T3);
	if(abs(0 - SensorValue[gyro])<20)
	{
		while(abs(SensorValue[leftdrive_enc]) < distance*direction && abs(SensorValue[rightdrive_enc]) < distance*direction)
		{
			if(time1[T3] > max_time)
			{
				break;
			}
			motor[drive_l1] = speed*direction;
			motor[drive_r1] = speed*direction;
			motor[drive_l2] = speed*direction;
			motor[drive_r2] = speed*direction;
			wait1Msec(20);
		}
		motor[drive_l1] = (speed*-direction)/9;
		motor[drive_r1] = (speed*-direction)/9;
		motor[drive_l2] = (speed*-direction)/9;
		motor[drive_r2] = (speed*-direction)/9;
		wait1Msec(250);
		motor[drive_l1] = 0;
		motor[drive_r1] = 0;
		motor[drive_l2] = 0;
		motor[drive_r2] = 0;
		return;
	}
	else
	{
		positiveTurn((0 - SensorValue[gyro]), 127, 200);
	}


}

void negativedistance(int distance, int speed, int max_time)
{
	SensorValue[leftdrive_enc] = 0;
	SensorValue[rightdrive_enc] = 0;
	int direction;
	if (distance < 0){direction = -1;}
	else if (distance > 0){direction = 1;}
	SensorValue[gyro] = 0;
	clearTimer(T3);
	if(abs(0 - SensorValue[gyro])<20)
	{
		while(SensorValue[rightdrive_enc] < distance*direction)
		{
			if(time1[T3] > max_time)
			{
				break;
			}
			motor[drive_l1] = speed*-direction;
			motor[drive_r1] = speed*-direction;
			motor[drive_l2] = speed*-direction;
			motor[drive_r2] = speed*-direction;
			wait1Msec(20);
		}
		motor[drive_l1] = (speed*-direction)/9;
		motor[drive_r1] = (speed*-direction)/9;
		motor[drive_l2] = (speed*-direction)/9;
		motor[drive_r2] = (speed*-direction)/9;
		wait1Msec(250);
		motor[drive_l1] = 0;
		motor[drive_r1] = 0;
		motor[drive_l2] = 0;
		motor[drive_r2] = 0;
		return;
	}
	else
	{
		negativeTurn((0 - SensorValue[gyro]), 127, 200);
	}


}


/*---------------------------------------------------------------------------*/
/*                          Pre-Autonomous Functions                         */
/*                                                                           */
/*  You may want to perform some actions before the competition starts.      */
/*  Do them in the following function.  You must return from this function   */
/*  or the autonomous and usercontrol tasks will not be started.  This       */
/*  function is only called once after the cortex has been powered on and    */
/*  not every time that the robot is disabled.                               */
/*---------------------------------------------------------------------------*/

void pre_auton()
{
	// Set bStopTasksBetweenModes to false if you want to keep user created tasks
	// running between Autonomous and Driver controlled modes. You will need to
	// manage all user created tasks if set to false.
	bStopTasksBetweenModes = true;

	// Set bDisplayCompetitionStatusOnLcd to false if you don't want the LCD
	// used by the competition include file, for example, you might want
	// to display your team name on the LCD in this function.
	// bDisplayCompetitionStatusOnLcd = false;

	// All activities that occur before the competition starts
	// Example: clearing encoders, setting servo positions, ...
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                              Autonomous Task                              */
/*                                                                           */
/*  This task is used to control your robot during the autonomous phase of   */
/*  a VEX Competition.                                                       */
/*                                                                           */
/*  You must modify the code to add your own robot specific commands here.   */
/*---------------------------------------------------------------------------*/

task autonomous()
{
	startTask(GetRPM); //Calculates RPM of flywheel
	FlywheelState=true;
	currentGoalVelocity = 2400; //Assigns desired RPM
	startTask(flywheelpower); //Assign flywheel motors and safe proof the motors
	startTask(Flywheel_Control); //Run the flywheel
	//startTask(autoShoot);
	idler=false;
	motor[intake]=-127; //Runs the intake Starts
	positivedistance(650,80,3000); //Goes towards the ball to intake
	motor[arm_cap1]=-30; //Arm is expanded starts
	motor[arm_cap2]=-30;
	wait1Msec(200);
	motor[arm_cap1]=30;
	motor[arm_cap2]=30;
	wait1Msec(200);
	motor[arm_cap1]=0;
	motor[arm_cap2]=0; //Arm is expanded ends
	motor[intake]=0; //Intake stops after the ball is in
	negativedistance(300,80,1700); //Reverses and realigns with wall
	positivedistance(20,50,600); //Drives slight forward away from the wall
	negativeTurn(400,50,1000); //Turns towards wall 1
	negativeTurn(400,35,1200); //Turns towards wall 2
	positivedistance(150,50,1200); //Moves forwards CHANGED
	positiveTurn(92,50,1000); //Turns slightly away from the wall
	positivedistance(30,50,600); //Moves closer to flags with the same angle
	motor[indexer]=-127; //Runs the indexer for the first ball = high flag
	wait1Msec(300);
	motor[indexer]=0; //Stops indexer
	positivedistance(120,50,1600); //Drive forward towards flags
	positiveTurn(30,55,500); //Turns slightly away from the wall
	wait1Msec(200); //Small pause
	motor[indexer]=-127; //Runs the indexer
	motor[intake]=-127; //Runs the intake
	wait1Msec(600); //Second ball shoot=middle flag
	motor[indexer]=0; //Stop indexer
	motor[intake]=0; //Stop intake
	wait1Msec(300);
	negativedistance(15,50,300); //Reverse before last turn
	negativeTurn(25,40,475); //Turns slightly towards the wall
	positivedistance(500,75,1400); //Drives forward and realigns
	negativedistance(70,70,700); //Reverse to match distance with cap
	motor[intake]=127; //Starts the intake to flip cap
	positiveTurn(755,50,2500); //Turns so the intake faces the cap
	positivedistance(500,127,3000); //Drives towards cap and flips it




	negativedistance(300,80,2000);
	positivedistance(20,50,600);
	positiveTurn(730,50,1500);
	positivedistance(645, 60, 5200);
	positiveTurn(740,50,1500);
	motor[arm_cap1]=-80; //Arm lifts slighlty to go up platform start
	motor[arm_cap2]=-80;
	wait1Msec(200);
	motor[arm_cap1]=0;
	motor[arm_cap2]=0; //Arm lifts slighlty to go up platform ends
	negativedistance(50,80,200);
	motor[drive_l1]=-127; //Drives up platform at full speed starts
	motor[drive_l2]=-127;
	motor[drive_r1]=-127;
	motor[drive_r2]=-127;
	wait1Msec(2250);
	motor[drive_l1]=20;
	motor[drive_l2]=20;
	motor[drive_r1]=20;
	motor[drive_r2]=20; //Drives up platform at full speed ends
	wait1Msec(200);
	motor[drive_l1]=0;
	motor[drive_l2]=0;
	motor[drive_r1]=0;
	motor[drive_r2]=0; //Drives up platform at full speed ends





}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                              User Control Task                            */
/*                                                                           */
/*  This task is used to control your robot during the user control phase of */
/*  a VEX Competition.                                                       */
/*                                                                           */
/*  You must modify the code to add your own robot specific commands here.   */
/*---------------------------------------------------------------------------*/

task usercontrol()
{


	while (true)
	{
		// This is the main execution loop for the user control program.
		// Each time through the loop your program should update motor + servo
		// values based on feedback from the joysticks.

		// ........................................................................
		// Insert user code here. This is where you use the joystick values to
		// update your motors, etc.
		// ........................................................................

		// Remove this function call once you have "real" code.
		UserControlCodePlaceholderForTesting();
	}
}
