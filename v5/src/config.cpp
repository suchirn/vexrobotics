#include "main.h"
#include "config.hpp"
#include <sstream>

pros::Controller master(pros::E_CONTROLLER_MASTER);
pros::Motor driveleft_B(13, pros::E_MOTOR_GEARSET_18, false,
                        pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor driveright_B(18, pros::E_MOTOR_GEARSET_18, false,
                         pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor driveleft_T(15, pros::E_MOTOR_GEARSET_18, true,
                        pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor driveright_T(17, pros::E_MOTOR_GEARSET_18, true,
                         pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor intake(11, pros::E_MOTOR_GEARSET_18, true,
                   pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor indexer(20, pros::E_MOTOR_GEARSET_18, true,
                    pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor flywheel(12, pros::E_MOTOR_GEARSET_06, true,
                     pros::E_MOTOR_ENCODER_COUNTS);
pros::Motor arm(19, pros::E_MOTOR_GEARSET_18, true,
                pros::E_MOTOR_ENCODER_COUNTS);
pros::ADIGyro gyro('A');
pros::ADIUltrasonic sensor(7, 8);

pros::Task drive_task(driveTask);
pros::Task turn_task(turnTask);

bool isGyroCalibrated = false;
bool isUserControl = false;

double gyroActual = 0;

void updateActualGyro(void* parameter) {
    while (!isGyroCalibrated) {
        pros::delay(20);
    }
    std::ostringstream str;

    double gyroFinal = gyro.get_value();
    double gyroInitial = 0;

    while (true) {
        gyroInitial = gyroFinal;
        pros::delay(20);
        gyroFinal = gyro.get_value();

        if (gyroFinal - gyroInitial > 100) {
            gyroActual += (gyroFinal - gyroInitial - 3600) * 0.942408376963351;
        } else if (gyroFinal - gyroInitial < -100) {
            gyroActual += (gyroFinal - gyroInitial + 3600) * 0.942408376963351;
        } else {
            gyroActual += (gyroFinal - gyroInitial) * 0.942408376963351;
        }

        if (gyroActual >= 3600)
            gyroActual -= 3600;
        else if (gyroActual <= -3600)
            gyroActual += 3600;

        str << gyroActual << std::endl;
        std::cout << "Actual: " << gyroActual << std::endl;
        std::cout << "Gyro: " << gyro.get_value() << std::endl;
        pros::lcd::set_text(1, str.str());

        str.str("");
    }
}


pros::Task tUpdateActualGyro{updateActualGyro};
